@extends('site.layouts.site')

@section('content')

<!-- ******  ****** -->
<!-- hero Section Top Slider -->
	@section('top_slider')
        @include('site.layouts.design.top_slider') 
    @show
<!-- End / hero  Section Top Slider -->
						
<!-- cta-02  Section Offer  -->
	@section('section_offer')
        @include('site.layouts.design.section_offer') 
    @show
<!-- End / cta-02 Section Offer-->
				
				
<!-- Section About  -->
	@section('section_home_about')
        @include('site.layouts.design.section_home_about') 
    @show
<!-- End / Section About -->			
				

<!-- Section Our Services -->
	@section('section_services')
        @include('site.layouts.design.section_services') 
    @show			
<!-- End / Section Our Services -->
				
				
<!-- Section  Gallery -->
	@section('section_gallery')
        @include('site.layouts.design.section_gallery') 
    @show
<!-- End / Section Gallery -->
				
				
<!-- Section Testimonial -->
	@section('section_testimonial')
        @include('site.layouts.design.section_testimonial') 
    @show			
<!-- End / Section Testimonial -->
				
				
<!-- Section Our Team -->
	@section('section_team')
        @include('site.layouts.design.section_team') 
    @show				
<!-- End / Section Our Team -->
				
				
<!-- Section Contact Us-->
	@section('section_contact')
        @include('site.layouts.design.section_contact') 
    @show				
<!-- End / Section Contact Us -->
       
<!-- ******  ****** -->
    
@endsection

		