<hero class="hero" id="id1">                    

<!-- swiper swiper-container -->
                    <div class="swiper swiper-container">
                        
@if(isset($slides) && is_object($slides))

     <div class="swiper-wrapper">

        @foreach($slides as $slide)

            <div class="hero__wrapper" style="background-image: url('{{asset('storage/images/slider/'.$slide->image)}}');">
                <div class="hero__inner">
                    <div class="container">
                        <h1 class="hero__title">{{ $slide->title }}</h1>
                        <p class="hero__desc">{{ $slide->subtitle }}</p>
                                            
                        <!-- btn -->
                        <a class="btn btn-primary" href="{{ url($slide->link_url) }}">Read now
                        </a><!-- End / btn -->
                                            
                    </div>
                </div>
            </div>

        @endforeach
                         
    </div>

@endif


                        <div class="swiper-pagination-custom"></div>
                        <div class="swiper-button-custom">
                            <div class="swiper-button-prev-custom"><i class="fa fa-angle-left"></i></div>
                            <div class="swiper-button-next-custom"><i class="fa fa-angle-right"></i></div>
                        </div>
                    </div><!-- End / swiper swiper-container -->
</hero>