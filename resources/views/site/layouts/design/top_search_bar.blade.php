<div class="btn-right">
    <div class="search-btn"><i class="fa fa-search"></i></div>
</div>
<div class="searchbar">
    <div class="searchbar__group">
        <span class="searchbar__addon"><i class="fa fa-search"></i></span>
        <input class="searchbar__input" type="text" name="search" value="" placeholder="Search"/><span class="searchbar__close"></span>
    </div>
</div>