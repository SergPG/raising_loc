<div class="footer-01__widget">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-lg-3 ">                
                @include('site.layouts.design.widget_about')                 
            </div>
            
            <div class="col-md-6 col-lg-3 ">
                @include('site.layouts.design.widget_tag')                      
            </div>

            <div class="col-md-6 col-lg-3 ">
                 @include('site.layouts.design.widget_flickr')                               
            </div>
                            
            <div class="col-lg-3 ">
                @include('site.layouts.design.widget_working_hours')             
            </div>

        </div>
    </div>
</div>
                
 <!-- copyright-01 -->
<div class="copyright-01 md-text-center">
    <div class="container">
        <p class="copyright-01__copy">2018 &copy; Copyright Awe7. All rights Reserved.</p>
    </div>
</div><!-- End / copyright-01 -->