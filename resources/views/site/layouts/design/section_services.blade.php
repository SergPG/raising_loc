
@if(isset($services) && is_object($services))

<section class="md-section" id="id3">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-2 ">
                                
        <!-- sec-title -->
        <div class="sec-title sec-title__lg-title md-text-center">
            <h2 class="sec-title__title">Our services</h2><span class="sec-title__divider"></span>
        </div><!-- End / sec-title -->
                                
      </div>
    </div>
                        
    <div class="row row-eq-height">

    @foreach($services as $service) 
       
      <div class="col-sm-6 col-md-6 col-lg-4 ">
                                
        <!-- services -->
        <div class="services">
          <div class="services__img"><img src="{{asset('storage/images/service/'.$service->image)}}" alt=""/></div>
          <h2 class="services__title"><a href="{{ url('/services/'.$service->id)}}">{{ $service->name }}</a></h2>
          <div class="services__desc">{{ $service->description }} </div>
                                    
          <!-- btn -->
          <a class="btn btn btn-primary btn-custom" href="{{ url('/services/'.$service->id)}}">read more
          </a><!-- End / btn -->
                                    
        </div><!-- End / services -->
                                
      </div>
    @endforeach 

    </div> <!-- End /row row-eq-height -->
  </div> <!-- End /container -->
</section>

@endif                