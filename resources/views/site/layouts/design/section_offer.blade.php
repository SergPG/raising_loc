<div class="cta-02">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 ">
                <h3 class="cta-02__title">Looking for a high quality constructor company for your project?</h3>
            </div>
            <div class="col-lg-3  md-text-right">
                                
                <!-- btn -->
                <a class="btn btn-outline" href="#">Get a quote
                </a><!-- End / btn -->
                                
            </div>
        </div>
    </div>
</div>