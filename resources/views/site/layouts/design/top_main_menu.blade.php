<nav class="raising-nav">
    <!-- raising-menu -->
    <ul class="raising-menu">
        <li class="current-menu-item"><a href="#id1">Home</a>
        </li>
        <li><a href="#id2">About</a>
        </li>
        <li><a href="#id3">Service</a>
        </li>
        <li><a href="#id4">Gallery</a>
        </li>
        <li><a href="#id5">Testimonial</a>
        </li>
        <li><a href="#id6">Team</a>
        </li>
        <li><a href="#id7">Contact us</a>
        </li>
    </ul><!-- raising-menu -->
                            
    <div class="navbar-toggle"><i class="fa fa-bars"></i></div>
</nav>
<!-- End / raising-nav -->