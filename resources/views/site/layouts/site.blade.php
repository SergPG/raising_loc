<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<!-- Fonts-->
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/fonts/fontawesome/font-awesome.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/fonts/pe-icon/pe-icon.css')}}">
		<!-- Vendors-->
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/vendors/bootstrap/grid.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/vendors/magnific-popup/magnific-popup.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/vendors/swiper/swiper.css')}}">
		<!-- App & fonts-->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald:400,600|Playfair+Display:400i">
		<link rel="stylesheet" type="text/css" href="{{asset('theme/site/assets/css/main.css')}}"><!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
	</head>
	
	<body>
				
				<!-- preload -->
				<div class="preload" id="preload">
					<div class="cssload-spin-box"></div>
				</div><!-- End / preload -->
				
		<div class="page-wrap">
			
			<!-- header -->
			<header class="header header-fixheight header--fixed">
				<div class="container">
					<div class="header__inner">

						<div class="header-logo"><a href="{{route('home')}}"><img src="{{asset('storage/images/logo_white.png')}}" alt=""/></a></div>
						
						<!-- Top raising_menu -->
						<!-- raising-nav -->
						
            				@section('top_main_menu')
              					 @include('site.layouts.design.top_main_menu') 
           					 @show 

          				<!-- END Top raising_menu -->		
						<!-- End / raising-nav -->
						
						<!-- Search Bar -->
            				@section('top_search_bar')
              					 @include('site.layouts.design.top_search_bar') 
           					 @show  
          				<!-- END Search Bar -->	

					</div>
				</div>
			</header><!-- End / header -->
			
			<!-- Content-->
			<div class="md-content">
			
			<!-- Main Content -->
			    @yield('content')
			<!-- END Main Content -->
		
			</div>
			<!-- End / Content-->
			

			<!-- Footer  -->
			<!-- footer-01 -->
			<footer class="footer-01 md-skin-dark">
                
            	@section('footer')
              		@include('site.layouts.design.footer') 
           		@show  
         		
			</footer><!-- End / footer-01 -->
			<!-- END Footer -->	
			
		</div>
		<!-- Vendors-->
		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/_jquery/jquery.min.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/imagesloaded/imagesloaded.pkgd.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/isotope-layout/isotope.pkgd.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/jquery.matchHeight/jquery.matchHeight.min.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/masonry-layout/masonry.pkgd.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/swiper/swiper.jquery.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/jquery-one-page/jquery.nav.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/menu/menu.js')}}"></script>

		<script type="text/javascript" src="{{asset('theme/site/assets/vendors/jquery.waypoints/jquery.waypoints.min.js')}}"></script>

		<!-- App-->
		<script type="text/javascript" src="{{asset('theme/site/assets/js/main.js')}}"></script>
	</body>
</html>