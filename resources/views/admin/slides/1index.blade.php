@extends('admin.layouts.admin')

@section('content')
   
   
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Slides</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

          
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">  

            <div class="panel-heading">
                <span class="pull-right">
                    <button id="btnNew" class="btn btn-warning mr-3">
                    <i class="fa fa-plus-circle fa-lg " aria-hidden="true"></i> New</button>
                </span>    
                <div class="clearfix"></div>
            </div>
            <!-- /.panel-heading -->  
     

            <div class="panel-body">

@if(isset($slides) && is_object($slides))

        
<!-- ******  Slides Table ****** -->
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-slides">

                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" width="19%">Image</th>
                                <th scope="col">Title</th>
                                <th scope="col" width="19%">Subtitle</th>
                                <th scope="col" >Link</th>
                                <th scope="col" id="Actions" >Actions</th>
                            </tr>
                        </thead>
                        <tbody id="slides-list" name="slides-list">

                         @foreach( $slides as $slide )  
   
                        <tr id="slide{{$slide->id}}">
                            <td scope="row">
                                <span>{{  $slide->id  }}</span></td>
                            
                            <td class="slide-image align-items-center" width="170">

                             <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo-{{  $slide->id  }}" title="View" >
                             	<i class="fa fa-search-plus" aria-hidden="true"></i> View
                            </button>
     
                                <p></p>
                                <div id="demo-{{  $slide->id  }}" class="collapse"> 
                                   <img src="{{asset('storage/images/slider').'/'.$slide->image}}" width="100%" height="100"  alt="Slide"> 
                                </div>
                                

                            </td>
                            <td class="slide-title "><span> {{  $slide->title  }}</span></td>
                            <td class="slide-remark" width="250"><span>{{  $slide->subtitle  }}</span></td>
                            <td class="slide-link-text ">
                                <a href="{{ url($slide->link_url) }}" class="btn btn-success" role="button" title="Read now" >
                                  <i class="fa fa-link" aria-hidden="true"></i>	
                                  <span> Read now</span>
                                </a>
                            </td>            
<td class="actions">
    <button id="btnEdit"  class="btn btn-primary mr-1" title="Edit" value="{{  $slide->id  }}" >
        <i class="fa fa-edit fa-lg" aria-hidden="true"></i>  Edit
    </button>

    <button  id="btnDelete" class="btn btn-danger  ml-1"  title="Delete" value="{{  $slide->id  }}">
        <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
    </button>
</td>
                        </tr>

                        @endforeach                      
                        
                        </tbody>
                    </table>
                        <!-- ****** END Welcome Slides Table ****** -->


            </div>
            <!-- /.panel-body --> 

        </div>
        <!-- /.panel panel-default --> 

    </div>
    <!-- /.ol-lg-12 -->
</div>
<!-- /.row -->
 @endif


<!-- ****** Quick View Modal Area Start ****** -->
<div class="modal fade" id="SlideView" tabindex="-1" role="dialog" aria-labelledby="SlideView" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        
        <div class="modal-content">
                    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Slide</h4>
            </div>
            <!-- / .modal-header -->  

<!-- Slide Form -->
<form id="frmSlide" name="frmSlide" class="form-horizontal" novalidate="" enctype="multipart/form-data">
            
            <div class="modal-body">    
                 
                <div class="form-group"> 
                    <label for="title" class="col-sm-3 control-label">Imaege</label> 
                    <div class="col-sm-9">                               
                        <img src="" width="100%" height="200"  alt="Slide" id="myImg"> 
                    </div> 
                    <input type="hidden" id="image_nameFile" name="image_nameFile" value="">
                </div>

                <div class="form-group">                   
                   <label for="title" class="col-sm-3 control-label"></label>
                   <div class="col-sm-9">
                     <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#imageFile" aria-expanded="false" aria-controls="imageFile">
                     Изменить</button>
                    </div> 
                </div>    
                <div class="form-group collapse" id="imageFile">                   
                   <label for="title" class="col-sm-3 control-label">File input</label>
                   <div class="col-sm-9">
                     <input type="file" class="form-control has-error" id="fileImage" 
data-namedirimage="{{ $nameDirImage }}" name="fileImage" >
                    </div> 
                </div>          
                
                <div class="form-group">                   
                   <label for="title_Slide" class="col-sm-3 control-label">Slide title</label>
                   <div class="col-sm-9">
                     <input type="text" class="form-control has-error" id="title_Slide" name="title" placeholder="Slide title" value="">
                     </div> 
                </div>     
                <div class="form-group">                   
                   <label for="subtitle_Slide" class="col-sm-3 control-label">Slide subtitle</label>
                   <div class="col-sm-9">
                     <input type="text" class="form-control has-error" id="subtitle_Slide" name="subtitle" placeholder="Slide subtitle" value="">
                   </div> 
                </div>   
                
                <div class="form-group">                   
                   <label for="link_url" class="col-sm-3 control-label">URL link</label>
                   <div class="col-sm-9">
                        <input type="text" class="form-control has-error" id="link_url" name="link_url" placeholder="Slide link url" value="">
                   </div> 
                </div>             
                                           

             </div>
             <!-- / .modal-body -->    

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

         <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>

         <input type="hidden" id="id_Slide" name="id_Slide" value="">
        
      </div>  

</form>
<!-- END Slide Form -->
                
        </div>
         <!-- / .modal-content --> 

    </div>
    <!-- / .modal-dialog -->   
   
</div>
<!-- ****** Quick View Modal Area End ****** -->


 <meta name="_token" content="{!! csrf_token() !!}" />
 

 <!-- ****** ******************** ****** -->   

 @push('scriptsSlides')


<script >


(function($) {
  // Start of use strict


  var url = "http://raising.loc/admin/";

$('#fileImage').change(function (e){ changeFile(e)});

$(document).on('click','#btnEdit',function(e){

	$('#frmSlide').trigger("reset");
	$('#imageFile').collapse('hide');
        
    var slide_id = $(this).val();
    console.log('Slide_id:', slide_id);

     $.get(url + 'slides/' + slide_id, function (data) {
            //success data
            console.log(data);

            var get_img = '<img src="http://raising.loc/storage/images/slider/'+ data.image +'" width="100%" height="200"  alt="Slide" id="myImg">  ';
             $("#myImg").replaceWith( get_img ); 

             $('#id_Slide').val(data.id);
             $('#image_nameFile').val(data.image);
             $('#title_Slide').val(data.title);
             $('#subtitle_Slide').val(data.subtitle);
             $('#link_url').val(data.link_url);
            $('#btn-save').val("update");
            $('#SlideView').modal('show');
        }) 
     // debugger;

    });  //end edit
///=============================

    // ==  display modal form for creating new slide
    $(document).on('click','#btnNew',function(){

        $('#btn-save').val("add");
        $('#frmSlide').trigger("reset");
		$('#imageFile').collapse('hide');

		var def_img = '<img src="http://raising.loc/storage/images/slider/img_def.jpg" width="100%" height="200"  alt="Slide" id="myImg">  ';
             $("#myImg").replaceWith( def_img ); 

        $('#SlideView').modal('show');
    });


    //delete Slide and remove it from list
    $(document).on('click','#btnDelete',function(){
        
        var slide_id = $(this).val();
       
        if(confirm("ВЫ ХОТИТЕ УДАЛИТЬ ЗАПИСЬ "+ slide_id + " ?"))
        {
             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: url + 'slides/' + slide_id,
                success: function (data) {
                    console.log(data);
                    $("#slide" + slide_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
         }; // end if
    });


    $(document).on('click','#btn-save',function(e){
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault(); 
  
          
  // debugger;

        var formData = {
            image: $('#image_nameFile').val(),
            title: $('#title_Slide').val(),
            subtitle: $('#subtitle_Slide').val(),
            link_url: $('#link_url').val(),
           }
          console.log(formData);

       debugger;
       
      //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();
        var my_url, type;

         if (state == "add"){
            type = "POST"; //for creating new resource
            my_url = url + 'slides';
            // debugger;
         };

         if (state == "update"){
            var slide_id = $('#id_Slide').val();
            type = "PUT"; //for updating existing resource
            my_url = url + 'slides/' + slide_id;
         };

        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            
            success: function (data) {
                console.log(data);

var slide_item = '<tr id="slide'+ data.id +'"> <td scope="row"><span>'+ data.id +'</span></td>';

  slide_item +='<td class="slide-image align-items-center" width="170">';  

  slide_item +='<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo-'+ data.id +'" title="View" ><i class="fa fa-search-plus" aria-hidden="true"></i> View </button> <p></p>'; 

  slide_item +='<div id="demo-'+ data.id +'" class="collapse">'; 

  slide_item +='<img src="http://raising.loc/storage/images/slider/'+ data.image +'" width="100%" height="100"  alt="Slide"> </div> ';  

  slide_item +='</td> <td class="slide-title "><span> '+ data.title +'</span></td>';

  slide_item +=' <td class="slide-remark" width="250"><span>'+ data.subtitle +'</span></td>';  

  slide_item +='<td class="slide-link-text "> <a href="'+ data.link_url +'" class="btn btn-success" role="button" title="Read now" ><i class="fa fa-link" aria-hidden="true"></i> <span> Read now</span> </a> </td>';  

  slide_item +='<td class="actions"> <button id="btnEdit"  class="btn btn-primary mr-1" title="Edit" value="'+ data.id +'" > <i class="fa fa-edit fa-lg" aria-hidden="true"></i>  Edit </button>';   

  slide_item +=' <button  id="btnDelete" class="btn btn-danger  ml-1"  title="Delete" value="'+ data.id +'"> <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete </button> </td> </tr> ';                              
                                  
  if (state == "add"){ //if user added a new record
      $('#slides-list').append(slide_item);
      }else{ //if user updated an existing record
      $("#slide" + slide_id).replaceWith( slide_item );
       }
                 
  $('#frmSlide').trigger("reset");
  $('#SlideView').modal('hide');

                
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


     
     });
///=============================

function changeFile(e){
  
    var nameDirImage = $('#fileImage').data("namedirimage");

  // debugger;

    $.ajaxSetup({
        headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
         }
        })
        e.preventDefault(); 

        var control = document.getElementById("fileImage");

        var formData = new FormData();       
          
           jQuery.each(control.files, function(i, file) {     
                 formData.append("fileImg", file);
             });

            console.log('nameDirImage', nameDirImage);

    ///===========    http://raising.loc/admin/previewImage
            $.ajax({
                type: "POST",
                url: "http://raising.loc/admin/previewImage/"+ nameDirImage,
                cache: false,
                contentType: false,
                processData: false,
                data: formData,

                dataType: 'json',
                success: function (data) {
                    console.log(data);

                  $('#image_nameFile').val(data.nameFile); 

        var my_img = '<img src="http://raising.loc/storage/images/tmp/'+ data.nameFile +'" width="100%" height="30%"  alt="Slide" id="myImg">  '; 

                 $("#myImg").replaceWith( my_img );     
                 
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

       } ///== END changeFile  ===



//function changeFile(e){

    
 //    var nameDirImage = $('#fileImage').data("namedirimage");

 //  // debugger;

	//  	// debugger;
 //    $.ajaxSetup({
	//     headers: {
	//      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	//      }
	//     })
	//     e.preventDefault(); 

	// 	var control = document.getElementById("fileImage");
    

	//  	var formData = new FormData();       
	      
	//        jQuery.each(control.files, function(i, file) {     
	//              formData.append("fileImg", file);
	//          });

  
	// ///===========    http://raising.loc/admin/previewImage
	//         $.ajax({
	//             type: "POST",
	//             url: "http://raising.loc/admin/previewImage/"+ nameDirImage,
	//             cache: false,
	//             contentType: false,
	//             processData: false,
	//             data: formData,

	//             dataType: 'json',
	//             success: function (data) {
	//                 console.log(data);

 //                  $('#image_nameFile').val(data.nameFile); 

	//     var my_img = '<img src="http://raising.loc/storage/images/'+ nameDirImage +'/'+ data.nameFile +'" width="100%" height="200"  alt="Slide" id="myImg">  ';
	//              $("#myImg").replaceWith( my_img );     
	             
	//             },
	//             error: function (data) {
	//                 console.log('Error:', data);
	//             }
	//         });

	 ///  } ///== END changeFile  ===

  })(jQuery); // End of use strict

</script>

 @endpush

@endsection
