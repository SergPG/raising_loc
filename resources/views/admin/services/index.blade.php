@extends('admin.layouts.admin')

@section('content')
   
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Services</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
       
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">  

            <div class="panel-heading">
                <span class="pull-right">
                     <a href="{{ url('admin/services/create') }}" id="btnNew" class="btn btn-warning mr-3" title="New" role="button"><i class="fa fa-plus-circle fa-lg " aria-hidden="true"></i> New</a>
                </span>    
                <div class="clearfix"></div>
            </div>
            <!-- /.panel-heading -->  
     

            <div class="panel-body">
        
<!-- ******  Services Table ****** -->
@if(isset($services) && is_object($services))

    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-services">

     <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col" width="19%">Image</th>
          <th scope="col">Name</th>
          <th scope="col" width="30%">Description</th>
          <th scope="col" id="Actions" >Actions</th>
        </tr>
     </thead>

    <tbody id="services-list" name="services-list">

        @foreach( $services as $service )  
   
          <tr id="service{{$service->id}}">
            <td scope="row">
              <span>{{  $service->id  }}</span></td>
                            
            <td class="service-image align-items-center" width="170">

            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo-{{  $service->id  }}" title="View" >
              <i class="fa fa-search-plus" aria-hidden="true"></i> View
            </button>
              <p></p>
            <div id="demo-{{  $service->id  }}" class="collapse"> 
              <img src="{{asset('storage/images/service').'/'.$service->image}}" width="100%" height="100"  alt="service"> 
            </div>
 
            </td>
              <td class="service-name "><span> {{  $service->name  }}</span></td>
              <td class="service-description" width="250"><span>{{  $service->description  }}</span></td>
                                    
<td class="actions">

    <a href="{{ url('admin/services/'.$service->id.'/edit') }}" id="btnEdit" class="btn btn-primary mr-1" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i>  Edit</a>


    <button  id="btnDelete" class="btn btn-danger  ml-1"  title="Delete" value="{{ $service->id  }}"  data-url="{{ url('admin/services') }}" >
        <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
    </button>
</td>
                        </tr>

                        @endforeach                      
                        
                        </tbody>
                    </table>
                        <!-- ****** END Welcome Slides Table ****** -->
@endif

            </div>
            <!-- /.panel-body --> 

        </div>
        <!-- /.panel panel-default --> 

    </div>
    <!-- /.ol-lg-12 -->
</div>
<!-- /.row -->

<meta name="_token" content="{!! csrf_token() !!}" />

@push('scriptsDelService')

<script >

(function($) {
  // Start of use strict

     //delete Slide and remove it from list
    $(document).on('click','#btnDelete',function(){
        
        var service_id = $(this).val();
        var url = $(this).data("url");
       
        if(confirm("ВЫ ХОТИТЕ УДАЛИТЬ ЗАПИСЬ "+ service_id + " ?"))
        {
             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: url + '/' + service_id,
                success: function (data) {
                    console.log(data);
                    $("#service" + service_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
         }; // end if
    });

  })(jQuery); // End of use strict

</script>

 @endpush


@endsection
