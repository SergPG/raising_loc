@extends('admin.layouts.admin')

@section('content')
   
   
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Services</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

          
<div class="row">
  <div class="col-lg-12">

    <!-- Form add/edit Service  -->
        @include('admin.layouts.design.formService') 
    <!-- END Form add/edit Service  -->     

  </div>
    <!-- /.ol-lg-12 -->
</div>
<!-- /.row -->
 

@endsection
