@extends('admin.layouts.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
 
    <!-- Information Card  -->
            @section('inform_cards')
                 @include('admin.layouts.design.inform_cards') 
            @show              
    <!-- Information Card  -->

        <div class="row">
            <div class="col-lg-8">
                                       
            <!-- Area Chart Example  -->
                @include('admin.layouts.design.area_chart_example') 
            <!-- END Area Chart Example  -->                     

            <!-- Bar Chart Example  -->
                @include('admin.layouts.design.bar_chart_example') 
            <!-- END Bar Chart Example  -->            

            <!-- Responsive Timeline  -->
                @include('admin.layouts.design.responsive_timeline') 
            <!-- END Responsive Timeline  -->  

            </div>
            <!-- /.col-lg-8 -->

            <div class="col-lg-4">
                    
                <!-- Notifications Panel  -->
                    @include('admin.layouts.design.notifications_panel') 
                <!-- END Notifications Panel  -->  

                <!-- Donut Chart Example  -->
                    @include('admin.layouts.design.donut_chart_example') 
                <!-- END Donut Chart Example  -->  

                <!-- Panel Chat   -->
                    @include('admin.layouts.design.panel_chat') 
                <!-- END Panel Chat  -->      

            </div>
            <!-- /.col-lg-4 -->

        </div>
        <!-- /.row -->
 


    @push('dashboard')

     <!-- Morris Charts JavaScript -->
    <script src="{{asset('theme/admin/vendor/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('theme/admin/vendor/morrisjs/morris.min.js')}}"></script>
    <script src="{{asset('theme/admin/data/morris-data.js')}}"></script>

    
    @endpush

@endsection

		