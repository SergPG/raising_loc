<div class="panel panel-default">  

      <div class="panel-heading">
        <h3> {{ ($form_status === 'add')? 'Create new' : 'Edit' }} service</h3>
      </div>
      <!-- /.panel-heading -->  

      <div class="panel-body">
        
        <form action=" {{ ($form_status === 'add')?  url('admin/services') :  url('admin/services/'.$service->id) }}" id="frmService" name="frmService" method="post" class="form-horizontal" novalidate="" enctype="multipart/form-data" role="form">
            {{ ($form_status === 'edit')? method_field('PUT') : '' }}

            {{ csrf_field() }}
            
           <div class="form-group"> 
                    <label for="title" class="col-sm-3 control-label">Imaege</label> 
                    <div class="col-sm-9">                               
                        <img src=" {{ ($form_status === 'edit')? url('storage/images/'.$nameDirImage.'/'.$service->image) : '' }}" width="100%" height="30%"  alt="" id="myImg"> 
                    </div> 
                    <input type="hidden" id="image_nameFile" name="image_nameFile" value="">
            </div>

            <div class="form-group">                   
                   <label for="title" class="col-sm-3 control-label"></label>
                   <div class="col-sm-9">
                     <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#imageFile" aria-expanded="false" aria-controls="imageFile">
                      {{ ($form_status === 'add')? 'New Image' : 'Change Image' }}
                    </button>
                    </div> 
                </div>    
                <div class="form-group collapse" id="imageFile">                   
                   <label for="title" class="col-sm-3 control-label">File input</label>
                   <div class="col-sm-9">
                     <input type="file" class="form-control has-error" id="fileImage" 
data-namedirimage="{{ $nameDirImage }}" name="fileImage" >
                    </div> 
                </div>          
                
            <div class="form-group">                   
              <label for="name_Service" class="col-sm-3 control-label">Service name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control has-error" id="name_Service" name="name" placeholder="Service name" value="{{ ($form_status === 'edit')? $service->name : '' }}">
                </div> 
            </div> 

            <div class="form-group">                   
              <label for="description_Service" class="col-sm-3 control-label">Service description</label>
                <div class="col-sm-9">
                  <textarea class="form-control has-error" id="description_Service" name="description" rows="3" placeholder="Service description">{{ ($form_status === 'edit')? $service->description : '' }}</textarea>
                </div> 
            </div>

            <div class="form-group">                   
              <label for="text_Service" class="col-sm-3 control-label">Service text</label>
                <div class="col-sm-9">
                  <textarea class="form-control has-error" id="text_Service" name="text" rows="7" placeholder="Service text">{{ ($form_status === 'edit')? $service->text : '' }}</textarea>
                </div> 
            </div>  

            <div class="form-group">                   
              <label for="text_Service" class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                  <button type="submit" class="btn btn-primary">
                    {{ ($form_status === 'add')? 'Create' : 'Save' }}
                  </button>  
                </div> 
            </div>   


      </div>
      <!-- /.panel-body --> 
      
      <div class="panel-footer"></div>
      <!-- /.panel-footer --> 

  </form>
    </div>
    <!-- /.panel panel-default --> 

    <meta name="_token" content="{!! csrf_token() !!}" />
 
 <!-- ****** ******************** ****** -->   

 @push('scriptsService')

<script >

(function($) {
  // Start of use strict

  var url = "http://raising.loc/admin/";
   
  $('#fileImage').change(function (e){ changeFile(e)}); 

///=============================

function changeFile(e){
  
    var nameDirImage = $('#fileImage').data("namedirimage");

  // debugger;

    $.ajaxSetup({
        headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
         }
        })
        e.preventDefault(); 

        var control = document.getElementById("fileImage");

        var formData = new FormData();       
          
           jQuery.each(control.files, function(i, file) {     
                 formData.append("fileImg", file);
             });

            console.log('nameDirImage', nameDirImage);

    ///===========    http://raising.loc/admin/previewImage
            $.ajax({
                type: "POST",
                url: "http://raising.loc/admin/previewImage/"+ nameDirImage,
                cache: false,
                contentType: false,
                processData: false,
                data: formData,

                dataType: 'json',
                success: function (data) {
                    console.log(data);

                  $('#image_nameFile').val(data.nameFile); 

        var my_img = '<img src="http://raising.loc/storage/images/tmp/'+ data.nameFile +'" width="100%" height="30%"  alt="Slide" id="myImg">  '; 

                 $("#myImg").replaceWith( my_img );     
                 
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

       } ///== END changeFile  ===

  })(jQuery); // End of use strict

</script>

 @endpush
