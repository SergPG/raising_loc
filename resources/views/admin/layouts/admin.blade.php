<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{asset('storage/images/core-img/favicon.ico')}}">

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('theme/admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('theme/admin/vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet">


    <!-- DataTables CSS -->
    <link href="{{asset('theme/admin/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet')}}">

    <!-- DataTables Responsive CSS -->
    <link href="{{asset('theme/admin/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet')}}">

     <!-- Social Buttons CSS -->
    <link href="{{asset('theme/admin/vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet')}}">


    <!-- Custom CSS -->
    <link href="{{asset('theme/admin/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('theme/admin/vendor/morrisjs/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('theme/admin/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >
  <div id="page-top"></div>

   <!-- #wrapper -->
  <div id="wrapper">

    <!-- Navigation Main-->

     @section('navigation_main')
         @include('admin.layouts.design.navigation_main') 
     @show     
        
    <!-- END Navigation Main -->

    <!-- Page Main Content -->
    <div id="page-wrapper">

        <!--  Content -->
             @yield('content')
        <!-- END  Content -->
            
    </div>
        <!-- /#page-wrapper -->
    <!-- END Page Main Content -->    
  
 </div>
    <!-- /#wrapper -->
    
     <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
      <!-- END Scroll to Top Button--> 

    <!-- jQuery -->
    <script src="{{asset('theme/admin/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('theme/admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('theme/admin/vendor/metisMenu/metisMenu.min.js')}}"></script>

   


     <!-- DataTables JavaScript -->
    <script src="{{asset('theme/admin/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('theme/admin/vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('theme/admin/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>


    <!-- Custom Theme JavaScript  -->   
    
    <script src="{{asset('theme/admin/dist/js/stylish-admin.js')}}"></script>                                        
    <script src="{{asset('theme/admin/dist/js/sb-admin-2.js')}}"></script>

    
   
    @stack('dashboard')
    
    @stack('scriptsSlides')
 
    @stack('scriptsService')

    @stack('scriptsDelService')
    
    

</body>

</html>

