<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//=============  Site ===========

Route::get('/', 'Site\HomeController@index')->name('home');
Route::post('/search', 'Site\HomeController@search')->name('home.search');
Route::post('/send', 'Site\HomeController@send')->name('home.send');

Route::get('/services/{id}', 'Site\ServiceController@show')->name('show.service');

//=============== Administration =========


//***************
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/','Admin\DashboardController@index')->name('admin');

	//Preview Image
	Route::post('previewImage/{nameDir?}','Admin\PreviewImageController@previewImage')->name('previewImage');

   //Slides 
	Route::resource('slides','Admin\SlidesController');
	

   //Services 
	Route::resource('services','Admin\ServiceController');	


//***************************************************


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home1');
