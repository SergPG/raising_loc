<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Site\SiteController;

use App\Models\Slide;
use App\Models\Service;


class HomeController extends SiteController
{
    //
     protected $modelSlide;
     protected $modelService;

     public function __construct(Slide $modelSlide, Service $modelService )
    {

        $this->modelSlide = $modelSlide;
        $this->modelService = $modelService;
    }



    public function index()
    {
      $slides = $this->modelSlide->all();
      $services = $this->modelService->all();
      

      /// dd($slides);

       return view('site.home.index')->with([
                        'slides' => $slides,
                        'services' => $services,
                        ]); ;

    }

     public function search(Request $request)
    {

    	
    }

     public function send(Request $request)
    {

    	
    }

}
