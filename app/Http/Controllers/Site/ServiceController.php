<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Site\SiteController;

use App\Models\Service;


class ServiceController extends SiteController
{
    
	protected $modelService;

     public function __construct(Service $modelService)
    {

        $this->modelService = $modelService;
    }


     public function show($id)
    {
        //
        $service = $id; //= $this->modelService->find($id);
        
       return view('site.service.show')->with([
                        'service' => $service,
                        ]); ;
    }



}
