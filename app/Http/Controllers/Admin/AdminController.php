<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    //
	protected $model;

	protected $nameDirImage;

	protected $path_Image = 'public/images/';




    //--------------------------------
	public function delete_tmp_image()
    {
       $file_all = Storage::allFiles('public/images/tmp');
       
       if($file_all){
           Storage::delete($file_all);
        }

    }


}
