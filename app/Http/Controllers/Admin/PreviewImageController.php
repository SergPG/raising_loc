<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreviewImageController extends Controller
{
    //
	  public function previewImage(Request $request, $nameDirImage)
    {
        //
        //$rand_num = rand(1, 999);
       // $path = 'public/images/'.$nameDirImage;
        $path = 'public/images/tmp';

       $f_img = $request->file('fileImg');
       $f_name = $f_img->getClientOriginalName();
       $f_ext = $f_img->getClientOriginalExtension();
       //$f_n = $rand_num.'_'.$f_img->getClientOriginalName();
       $f_url = $f_img->storeAs($path,$f_name);
      
      $result = [ 'extensionFile' => $f_ext,
                  'nameFile' => $f_name,
                  'urlFile' => $f_url,
                 ] ;

      return response()->json($result);
    }
}
