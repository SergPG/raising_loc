<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Models\Service;

use Illuminate\Support\Facades\Storage;

class ServiceController extends AdminController
{
    protected $services;
    protected $service;


     public function __construct(Service $model)
    {

        $this->model = $model;

        $this->nameDirImage = 'service';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->services = $this->model->all();

        return view('admin.services.index')->with([
                        'services' => $this->services,
                        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $form_status = 'add';
         return view('admin.services.create_edit_service')->with([
                        'form_status' => $form_status,
                        'nameDirImage' => $this->nameDirImage,
                        ]);  
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
         $path_img = $this->path_Image.$this->nameDirImage; 
         $num = ($this->model->max('id'))+1;

          $file_img = $request->file('fileImage'); 
            $ext_img = $file_img->getClientOriginalExtension();
            //  $name_img = $file_img->getClientOriginalName();

         $service_image_name = rand(1, 999).'service_'.$num.'.'.$ext_img;
            $file_img->storeAs($path_img,$service_image_name);

         $this->model->image = $service_image_name;
         $this->model->name = $request->name;
         $this->model->description = $request->description;
         $this->model->text = $request->text;
         $this->model->alias = 'service_'.$num;

         $this->model->save(); 


          $this->delete_tmp_image();

        return redirect('admin/services'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         //
         $form_status = 'edit';
         $this->service = $this->model->find($id);


         return view('admin.services.create_edit_service')->with([
                        'form_status' => $form_status,
                        'nameDirImage' => $this->nameDirImage,
                        'service' => $this->service,
                        ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $this->service = $this->model->find($id);
         $old_img = $this->service->image;


        if ($request->has('fileImage')) {

            $path_img = $this->path_Image.$this->nameDirImage; 

            $file_img = $request->file('fileImage');

            $ext_img = $file_img->getClientOriginalExtension();

            $service_image_name = rand(1, 999).'service_'.$id.'.'.$ext_img;

            $file_img->storeAs($path_img,$service_image_name);

            Storage::delete($this->path_Image.$this->nameDirImage.'/'.$old_img);

           $this->service->image = $service_image_name;
        }
        
         $this->service->name = $request->name;
         $this->service->description = $request->description;
         $this->service->text = $request->text; 

         $this->service->save(); 

          $this->delete_tmp_image();

        return redirect('admin/services'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->service = $this->model->find($id);
        $old_img = $this->service->image;
        Storage::delete($this->path_Image.$this->nameDirImage.'/'.$old_img);
      
      $this->service = $this->model->destroy($id);  

      return response()->json($this->service);
    }
    
   


}
