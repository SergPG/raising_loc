<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;

use Illuminate\Support\Facades\Storage;

use App\Models\Slide;

class SlidesController extends AdminController
{

    protected $slides;
    protected $slide;
   
    protected $nameDirImage = 'slider';

     public function __construct(Slide $model)
    {

        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->slides = $this->model->all();

        return view('admin.slides.index')->with([
                        'slides' => $this->slides,
                        'nameDirImage' => $this->nameDirImage,
                        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $path_img = $this->path_Image.$this->nameDirImage; 
        $num = ($this->model->max('id'))+1;
        $ext_img = $request->extensionFile;

         $slide_image_name = rand(1, 999).'slide_'.$num.'.'.$ext_img;

        $exists = Storage::exists($this->path_Image.'tmp/'.$request->image);

        Storage::copy($this->path_Image.'tmp/'.$request->image, $this->path_Image.$this->nameDirImage.'/'.$slide_image_name);

//=========================================================
         $this->model->image = $slide_image_name; 
          $this->model->title = $request->title;
           $this->model->subtitle = $request->subtitle;
            $this->model->link_url = $request->link_url;
         
          $this->model->save();  

        $this->delete_tmp_image();  
        
        return response()->json($this->model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->slide = $this->model->find($id);
        
        return response()->json($this->slide);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->slide = $this->model->find($id);
        $old_img = $this->slide->image;
   
        if ($request->has('extensionFile') ){

             $path_img = $this->path_Image.$this->nameDirImage; 

             $ext_img = $request->extensionFile;
             $slide_image_name = rand(1, 999).'slide_'.$id.'.'.$ext_img;

        Storage::delete($this->path_Image.$this->nameDirImage.'/'.$old_img);
      
     Storage::copy($this->path_Image.'tmp/'.$request->image, $this->path_Image.$this->nameDirImage.'/'.$slide_image_name);

              $this->slide->image = $slide_image_name;
              $this->slide->title = $request->title;
              $this->slide->subtitle = $request->subtitle;
              $this->slide->link_url = $request->link_url;
            $this->slide->save();   

            $this->delete_tmp_image();   

        }
         else{
                $this->slide->image = $old_img;
                $this->slide->title = $request->title;
                $this->slide->subtitle = $request->subtitle;
                $this->slide->link_url = $request->link_url;
             $this->slide->save();   
     
         }
    
        return response()->json($this->slide);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->slide = $this->model->find($id);
        $old_img = $this->slide->image;
        Storage::delete($this->path_Image.$this->nameDirImage.'/'.$old_img);
      
      $this->slide = $this->model->destroy($id);  

      return response()->json($this->slide);
    } 

    
}
